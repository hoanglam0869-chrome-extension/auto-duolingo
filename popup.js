import { BASE_URL, AUTO_URL, Auto, Repeat, AutoTab } from "./utils/constant.js";

const checkBoxes = document.querySelectorAll("input[type='checkbox']");
const radioBtns = document.querySelectorAll("input[type='radio']");
const speedLabel = document.querySelector("label[for='speed']");
const speedRng = document.querySelector("input[type='range']");
const speedText = ["Very slow", "Slow", "Normal", "Fast", "Very fast"];
const quantityBox = document.querySelector("#quantity");
const timerBoxes = document.querySelectorAll(".timer");
const progress = document.querySelector("#progress");
const progress2 = document.querySelector("#progress-2");
let interval;

chrome.storage.local.get(["auto", "repeat"], ({ auto, repeat }) => {
  setup(auto, repeat);
});

checkBoxes.forEach((checkBox) => {
  checkBox.oninput = () => {
    checkBoxes.forEach((cb) => (cb.checked = false));
    checkBox.checked = true;
  };
});

radioBtns.forEach((radioBtn) => {
  radioBtn.oninput = () => {
    radioBtns.forEach((rb) => (rb.checked = false));
    radioBtn.checked = true;
  };
});

speedRng.onchange = () => {
  speedLabel.innerText = `Speed - ${speedText[speedRng.value]}`;
};

document.onkeyup = (e) => {
  if (e.key == "Enter") {
    let main = document.querySelector("#main");
    let auto = document.querySelector("#auto");
    if (main.style.display == "block") {
      start();
    } else if (auto.style.display == "block") {
      stop();
    } else {
      back();
    }
  }
};

document.querySelector("#start").onclick = () => start();

function start() {
  let autoType = Array.from(checkBoxes).findIndex((cb) => cb.checked);
  let auto = new Auto(autoType, true, Number(speedRng.value));
  auto.saveAuto();

  const time = Array.from(timerBoxes).map((t) =>
    isNaN(t.value) ? 5 : t.value > 59 ? 59 : t.value < 0 ? 0 : t.value
  );
  const total = time.reduce((n, t, i) => {
    return n + t * Math.pow(60, time.length - 1 - i) * 1000;
  }, 0);

  let repeatType = Array.from(radioBtns).findIndex((rb) => rb.checked);
  let val = quantityBox.value;
  let max = isNaN(val) ? 500 : val > 9999 ? 9999 : val < 1 ? 1 : val;
  let limit = Date.now() + total;

  let repeat = new Repeat(repeatType, 1, Number(max), time, limit, 0, 0);
  repeat.saveRepeat();

  handleAuto(repeat);

  chrome.tabs.query({}, function (tabs) {
    let tab = tabs.find((t) => t.url == AUTO_URL[autoType]);
    if (tab) {
      chrome.tabs.sendMessage(tab.id, { message: "auto" });
    } else {
      tab = tabs.find((t) => t.url.includes(BASE_URL));
      if (tab) {
        chrome.tabs.update(tab.id, { active: true, url: AUTO_URL[autoType] });
      } else {
        chrome.tabs.create({ url: AUTO_URL[autoType] }, (t) => (tab = t));
      }
    }
    AutoTab.create(tab).saveAutoTab();
  });
}

document.querySelector("#stop").onclick = () => stop();

function stop() {
  clearInterval(interval);
  chrome.storage.local.get(["auto", "repeat"], (result) => {
    let auto = Auto.create(result.auto).setIsAuto(false);
    auto.saveAuto();

    displayTab("#main", "#auto");
    setup(auto, result.repeat);
  });
}

document.querySelector("#back").onclick = () => back();

function back() {
  chrome.storage.local.get(["auto", "repeat"], ({ auto, repeat }) => {
    displayTab("#main", "#result");
    setup(auto, repeat);
  });
}

function setup(auto, repeat) {
  const { autoType, isAuto, speed } = auto;
  if (isAuto) {
    handleAuto(repeat);
  } else {
    checkBoxes[autoType].checked = true;
    speedLabel.innerText = `Speed - ${speedText[speed]}`;
    speedRng.value = speed;

    const { repeatType, quantity, timer } = repeat;
    radioBtns[repeatType].checked = true;

    quantityBox.value = quantity.max;
    timerBoxes.forEach((t, i) => (t.value = timer.time[i]));
  }
}

function handleAuto({ repeatType, quantity, timer, xp, correctPercent }) {
  displayTab("#auto", "#main");

  if (repeatType == 0) {
    progress.innerText = `${quantity.current}/${quantity.max}`;
  } else {
    progress.innerText = formatTime(timer);
    interval = setInterval(() => {
      progress.innerText = formatTime(timer);
    }, 1000);
  }
  document.querySelector("#xp").innerText = xp;
  document.querySelector("#correct").innerText = `${correctPercent}%`;
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.message == "quantity") {
    let { current, max } = request.payload;
    progress.innerText = `${current}/${max}`;
  }
  if (request.message == "finish") {
    result();
  }
  if (request.message == "result") {
    document.querySelector("#xp").innerText = request.payload.xp;
    document.querySelector("#correct").innerText = `${request.payload.cp}%`;
  }
  return true;
});

function result() {
  clearInterval(interval);
  chrome.storage.local.get(["auto", "repeat"], ({ auto, repeat }) => {
    Auto.create(auto).setIsAuto(false).saveAuto();

    displayTab("#result", "#auto");

    let { repeatType, quantity, timer, xp, correctPercent } = repeat;
    if (repeatType == 0) {
      progress2.innerText = `${quantity.current}/${quantity.max}`;
    } else {
      progress2.innerText = formatTime(timer);
    }
    document.querySelector("#xp-2").innerText = xp;
    document.querySelector("#correct-2").innerText = `${correctPercent}%`;
  });
}

function formatTime(timer) {
  const d = (timer.limit - Date.now()) / 1000;
  if (d < 0) return "00:00:00";
  const h = Math.floor(d / 3600);
  const m = Math.floor((d - h * 3600) / 60);
  const s = Math.floor(d - h * 3600 - m * 60);
  return [h, m, s].map((t) => (t < 10 ? `0${t}` : t)).join(":");
}

function displayTab(block, none) {
  document.querySelector(block).style.display = "block";
  document.querySelector(none).style.display = "none";
}
