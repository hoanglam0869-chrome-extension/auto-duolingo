let val, currentURL, delay;
let practiceIndex = 0;
let totalQuestions = 1;
// Nếu người dùng bấm Stop khi đang auto, sau đó bấm Play thì extension vẫn auto, không làm bài test mới.
let isLessonFinished = true;
//
const btnsClN = ".WxjqG._1Qh5D._36g4N._2YF0P";
const regex = new RegExp(" *- *", "g");
const inputEvent = new Event("input", { bubbles: true });

(async () => {
  let src = chrome.runtime.getURL("utils/constant.js");
  val = await import(src);

  chrome.storage.local.get(["auto"], ({ auto }) => {
    const { autoType, isAuto, speed } = auto;
    if (!isAuto) return;
    currentURL = val.AUTO_URL[autoType];
    delay = val.delay(speed);
    start();
  });
})();

function start() {
  switch (window.location.href) {
    case val.AUTO_URL[0]:
      startPractice();
      break;
    case val.AUTO_URL[1]:
      startListening();
      break;
    default:
      break;
  }
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.message == "auto") {
    if (isLessonFinished) start();
  }
});

function handleAuto() {
  chrome.storage.local.get(["auto", "repeat"], (result) => {
    let auto = val.Auto.create(result.auto);
    let repeat = val.Repeat.create(result.repeat);

    const { autoType, isAuto } = auto;
    if (!isAuto) return;
    currentURL = val.AUTO_URL[autoType];
    const { repeatType, quantity, timer } = repeat;
    if (repeatType == 0) {
      let { current, max } = quantity;
      if (current < max) {
        repeat.setQuantity(++current, max).saveRepeat();

        sendMessage("quantity", { current: current, max: max });
        window.location.href = currentURL;
      } else {
        sendMessage("finish");
        auto.setIsAuto(false).saveAuto();
      }
    } else {
      if (Date.now() < timer.limit) {
        window.location.href = currentURL;
      } else {
        sendMessage("finish");
        auto.setIsAuto(false).saveAuto();
      }
    }
  });
}

function startListening() {
  val.findDOM("[data-test='player-next']", (nextBtn) => {
    nextBtn.click();
    setTimeout(startPractice, delay.delayToNewTest);
  });
}

function startPractice() {
  const xpEl = document.querySelector("._23i43");
  const nextBtn = document.querySelector(
    "[data-test='player-next'][aria-disabled='false']"
  );
  const noThanksBtn = document.querySelector(
    "[data-test='practice-hub-ad-no-thanks-button']"
  );
  const plusNoThanksBtn = document.querySelector(
    "[data-test='plus-no-thanks']"
  );
  const monthChalBtn = document.querySelector(
    "._2G7zH._3HhhB._2NolF._275sd._1ZefG._14BAU"
  );
  if (window.location.href != currentURL) {
    isLessonFinished = true;
    handleAuto();
  } else if (xpEl) {
    chrome.runtime.sendMessage({ message: "get_xp" }, (xp) => {
      chrome.storage.local.get(["repeat"], (result) => {
        let repeat = val.Repeat.create(result.repeat);

        let totalXP = repeat.xp + Number(xp);
        sendMessage("result", { xp: totalXP, cp: 100 });
        repeat.setXP(totalXP).setCorrectPercent(100).saveRepeat();

        setTimeout(() => {
          nextBtn.click();
          setTimeout(startPractice, delay.delayToNewTest);
        }, delay.delayToNext);
      });
    });
  } else if (nextBtn) {
    nextBtn.click();
    setTimeout(startPractice, delay.delayToNewTest);
  } else if (noThanksBtn) {
    noThanksBtn.click();
    setTimeout(startPractice, delay.delayToNewTest);
  } else if (plusNoThanksBtn) {
    plusNoThanksBtn.click();
    setTimeout(startPractice, delay.delayToNewTest);
  } else if (monthChalBtn) {
    monthChalBtn.click();
    setTimeout(startPractice, delay.delayToNewTest);
  } else {
    val.findDOM("._1XNQX", (_) => {
      chrome.runtime.sendMessage({ message: "get_challenge" }, (challenge) => {
        // if (totalQuestions === 1) {
        let progressBar = document.querySelector('[role="progressbar"]');
        let valueNow = progressBar.getAttribute("aria-valuenow");
        if (valueNow > 0) {
          let valueMax = progressBar.getAttribute("aria-valuemax");
          totalQuestions = (practiceIndex * valueMax) / valueNow;
        }
        // }
        if (practiceIndex < totalQuestions) {
          console.log(`${++practiceIndex}/${totalQuestions}`, challenge.type);
          isLessonFinished = false;

          switch (challenge.type) {
            case "listenMatch":
              listenMatch();
              break;
            case "assist":
              assist(challenge);
              break;
            case "translate":
              translate(challenge);
              break;
            case "listenIsolation":
              listenIsolation(challenge);
              break;
            case "speak":
              speak(challenge);
              break;
            case "listenComplete":
              listenComplete(challenge);
              break;
            case "partialReverseTranslate":
              partialReverseTranslate(challenge);
              break;
            case "listen":
              listen(challenge);
              break;
            case "name":
              _name(challenge);
              break;
            case "gapFill":
              gapFill(challenge);
              break;
            case "form":
              form(challenge);
              break;
            case "listenTap":
              listenTap(challenge);
              break;
            default:
              break;
          }
        } else {
          setTimeout(startPractice, delay.delayToNewTest);
        }
      });
    });
  }
}
// Nhấn vào các cặp tương ứng
// Nghe rồi chọn cặp từ tương ứng
function listenMatch() {
  const btns = document.querySelectorAll(btnsClN);
  const getAnswer = (index) => btns[index].getAttribute("data-test");

  let count = 0;
  const len = btns.length;

  const doTest = (count) => {
    for (let i = len / 2; i < len; i++) {
      if (getAnswer(count) == getAnswer(i)) {
        btns[count].click();
        btns[i].click();
        break;
      }
    }
    setTimeout(() => {
      if (++count < len / 2) {
        doTest(count);
      } else {
        document.querySelector(nextBtnClN)?.click();
        setTimeout(startPractice, delay.delayToNewTest);
      }
    }, delay.delayToMatch);
  };
  doTest(count);
}
// Chọn từ tiếng Anh theo nghĩa tiếng Việt
function assist(challenge) {
  console.log(challenge.prompt, challenge.choices[challenge.correctIndex]);
  const choices = document.querySelectorAll("[data-test='challenge-choice']");

  choices[challenge.correctIndex].click();
  nextQuestion();
}
// Viết lại bằng Tiếng Anh/Tiếng Việt
// Loại 1: Sắp xếp câu dịch theo một câu cho trước
// Loại 2: Viết câu dịch theo một câu cho trước
function translate(challenge) {
  console.log(challenge.prompt, challenge.correctSolutions);
  const input = document.querySelector(
    "[data-test='challenge-translate-input']"
  );

  if (input) {
    console.log("Viết câu");
    input.value = challenge.correctSolutions[0];
    input.dispatchEvent(inputEvent);

    nextQuestion();
  } else {
    console.log("Sắp xếp từ");
    let tokens = challenge.correctTokens;
    const wordBank = document.querySelector("[data-test='word-bank']");

    let count = 0;
    const doTest = (count) => {
      const btns = wordBank.querySelectorAll(btnsClN);
      for (let i = 0; i < btns.length; i++) {
        if (compareStr(tokens[count], btns[i].innerText)) {
          btns[i].click();
          break;
        }
      }
      setTimeout(() => {
        if (++count < tokens.length) {
          doTest(count);
        } else {
          nextQuestion();
        }
      }, delay.delayToMatch);
    };
    doTest(count);
  }
}
// Nghe và tìm từ còn thiếu
// Nghe từng đáp án rồi chọn để điền vào chỗ trống
function listenIsolation(challenge) {
  console.log(challenge.solutionTranslation);
  const choices = document.querySelectorAll("[data-test='challenge-choice']");

  choices[challenge.correctIndex].click();
  nextQuestion();
}
// Viết lại bằng tiếng Việt
// Nghe rồi sắp xếp câu dịch
function speak(challenge) {
  console.log(challenge.prompt, challenge.solutionTranslation);

  const skipBtn = document.querySelector("[data-test='player-skip']");
  if (skipBtn) {
    skipBtn.click();

    setTimeout(() => {
      document.querySelector(nextBtnClN)?.click();
      setTimeout(startPractice, delay.delayToNewTest);
    }, delay.delayToNext);
  } else {
    const wordBank = document.querySelector("[data-test='word-bank']");
    let tokens = challenge.solutionTranslation.replace(regex, " ").split(" ");

    let count = 0;
    const doTest = (count) => {
      const btns = wordBank.querySelectorAll("._1rl91.WOZnx._275sd._1ZefG");
      for (let i = 0; i < btns.length; i++) {
        if (compareStr(tokens[count], btns[i].innerText)) {
          btns[i].click();
          break;
        }
      }
      setTimeout(() => {
        if (++count < tokens.length) {
          doTest(count);
        } else {
          nextQuestion();
        }
      }, delay.delayToMatch);
    };
    doTest(count);
  }
}
// Nhập từ còn thiếu
// Nghe và điền từ tiếng Anh vào chỗ trống
function listenComplete(challenge) {
  console.log(challenge.solutionTranslation);

  let answer = challenge.displayTokens
    .map((token) => (token.isBlank ? token.text : ""))
    .join(" ")
    .trim();

  const input = document.querySelector("[data-test='challenge-text-input']");
  input.value = answer;
  input.dispatchEvent(inputEvent);

  nextQuestion();
}
// Hoàn thành câu
// Hoàn thành câu tiếng Anh từ câu tiếng Việt
function partialReverseTranslate(challenge) {
  console.log(challenge.prompt);

  let answer = challenge.displayTokens
    .map((token) => (token.isBlank ? token.text : ""))
    .join(" ")
    .trim();

  val.findDOM("._2EMkI._2i8FG._32bZV.tapBI._1W1IX", (input) => {
    input.innerText = answer;
    input.dispatchEvent(inputEvent);

    nextQuestion();
  });
}
// Nhập lại nội dung bạn vừa nghe
function listen(challenge) {
  console.log(challenge.prompt, challenge.solutionTranslation);

  const input = document.querySelector(
    "[data-test='challenge-translate-input']"
  );
  input.value = challenge.prompt;
  input.dispatchEvent(inputEvent);

  nextQuestion();
}
// Dịch ... sang Tiếng Anh
// Cho từ tiếng Việt rồi dịch sang tiếng Anh
function _name(challenge) {
  console.log(challenge.prompt, challenge.correctSolutions);

  const input = document.querySelector("[data-test='challenge-text-input']");
  input.value = challenge.correctSolutions[0];
  input.dispatchEvent(inputEvent);

  nextQuestion();
}
// Điền vào chỗ trống
// Chọn từ điền vào chỗ trống
function gapFill(challenge) {
  const btns = document.querySelectorAll("[data-test='challenge-choice']");
  btns[challenge.correctIndex].click();

  nextQuestion();
}
// Chọn từ còn thiếu
// Chọn từ điền vào chỗ trống
function form(challenge) {
  const btns = document.querySelectorAll("[data-test='challenge-choice']");
  btns[challenge.correctIndex].click();

  nextQuestion();
}
// Nghe và sắp xếp
function listenTap(challenge) {
  let tokens = challenge.correctTokens;
  const wordBank = document.querySelector("[data-test='word-bank']");

  let count = 0;
  const doTest = (count) => {
    const btns = wordBank.querySelectorAll(btnsClN);
    for (let i = 0; i < btns.length; i++) {
      if (compareStr(tokens[count], btns[i].innerText)) {
        btns[i].click();
        break;
      }
    }
    setTimeout(() => {
      if (++count < tokens.length) {
        doTest(count);
      } else {
        nextQuestion();
      }
    }, delay.delayToMatch);
  };
  doTest(count);
}

const nextBtnClN = "[data-test='player-next'][aria-disabled='false']";

function nextQuestion() {
  document.querySelector(nextBtnClN)?.click();

  setTimeout(() => {
    document.querySelector(nextBtnClN)?.click();
    setTimeout(startPractice, delay.delayToNewTest);
  }, delay.delayToNext);
}

function compareStr(token, text) {
  const str1 = token.trim();
  const str2 = text.trim();

  if (str1 == str2) return true;
  if (str1 == `${str2}.`) return true;
  if (str1 == `${str2}?`) return true;
  if (str1 == `${str2},`) return true;
  if (str1 == `${str2}!`) return true;

  return false;
}

function FindReact(dom, traverseUp = 0) {
  const key = Object.keys(dom).find((key) => key.startsWith("__reactProps$"));
  return dom[key].children._owner.memoizedProps.currentChallenge;
}

function sendMessage(msg, payload) {
  chrome.runtime.sendMessage({ message: msg, payload: payload });
}
