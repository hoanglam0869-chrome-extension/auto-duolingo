export const listenMatch = async (practiceIndex) => {
  const btns = document.querySelectorAll("._1rl91.WOZnx._275sd._1ZefG");

  const getAnswer = (index) => btns[index].getAttribute("data-test");

  let count = 0;
  const len = btns.length;

  const doTest = (count) => {
    for (let i = len / 2; i < len; i++) {
      if (getAnswer(count) == getAnswer(i)) {
        btns[count].click();
        btns[i].click();
        break;
      }
    }
    setTimeout(() => {
      if (++count < len / 2) {
        doTest(count);
      } else {
        chrome.storage.local.set({ practiceIndex: ++practiceIndex });
        document.querySelector("[data-test='player-next']").click();
      }
    }, 2000);
  };

  doTest(count);
};
