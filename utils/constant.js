export const listenMatch = "listenMatch";
export const assist = "assist";
export const translate = "translate";
export const listenIsolation = "listenIsolation";
export const speak = "speak";
export const listenComplete = "listenComplete";
export const partialReverseTranslate = "partialReverseTranslate";
export const listen = "listen";
export const name = "name";
export const gapFill = "gapFill";
export const form = "form";

export const BASE_URL = "https://www.duolingo.com";
export const AUTO_URL = [
  `${BASE_URL}/practice`,
  `${BASE_URL}/practice-hub/listening-practice`,
];

const delayToMatch = [1875, 750, 300, 120, 48]; // 48
const delayToNext = [3750, 1500, 600, 240, 96];
const delayToNewTest = [18750, 7500, 3000, 1200, 480];

export const delay = (speed) => ({
  delayToMatch: delayToMatch[speed],
  delayToNext: delayToNext[speed],
  delayToNewTest: delayToNewTest[speed],
});

export function findDOM(selector, callback) {
  let element = document.querySelector(selector);
  if (element) {
    callback(element);
  } else {
    let interval = setInterval(() => {
      element = document.querySelector(selector);
      if (element) {
        clearInterval(interval);
        callback(element);
      }
    }, 1000);
  }
}

export class Auto {
  constructor(autoType = 0, isAuto = false, speed = 2) {
    this.autoType = autoType;
    this.isAuto = isAuto;
    this.speed = speed;
  }

  setAutoType(autoType = 0) {
    this.autoType = autoType;
    return this;
  }

  setIsAuto(isAuto = false) {
    this.isAuto = isAuto;
    return this;
  }

  setSpeed(speed = 2) {
    this.speed = speed;
    return this;
  }

  saveAuto() {
    chrome.storage.local.set({ auto: this });
  }

  static create(auto) {
    let { autoType, isAuto, speed } = auto;
    return new Auto(autoType, isAuto, speed);
  }
}

export class Repeat {
  constructor(
    repeatType = 0,
    current = 1,
    max = 500,
    time = [5, 5, 5],
    limit = 0,
    xp = 0,
    correctPercent = 0
  ) {
    this.repeatType = repeatType;
    this.quantity = new Quantity(current, max);
    this.timer = new Timer(time, limit);
    this.xp = xp;
    this.correctPercent = correctPercent;
  }

  setRepeatType(repeatType = 0) {
    this.repeatType = repeatType;
    return this;
  }

  setQuantity(current = 1, max = 500) {
    this.quantity = new Quantity(current, max);
    return this;
  }

  setTimer(time = [5, 5, 5], limit = 0) {
    this.timer = new Timer(time, limit);
    return this;
  }

  setXP(xp = 0) {
    this.xp = xp;
    return this;
  }

  setCorrectPercent(correctPercent = 0) {
    this.correctPercent = correctPercent;
    return this;
  }

  saveRepeat() {
    chrome.storage.local.set({ repeat: this });
  }

  static create(repeat) {
    let { repeatType, quantity, timer, xp, correctPercent } = repeat;
    return new Repeat(
      repeatType,
      quantity.current,
      quantity.max,
      timer.time,
      timer.limit,
      xp,
      correctPercent
    );
  }
}

export class AutoTab {
  constructor(tabId = 0) {
    this.tabId = tabId;
  }

  saveAutoTab() {
    chrome.storage.local.set({ autoTab: this });
  }

  static create(autoTab) {
    return new AutoTab(autoTab.id);
  }
}

class Quantity {
  constructor(current, max) {
    this.current = current;
    this.max = max;
  }
}

class Timer {
  constructor(time, limit) {
    this.time = time;
    this.limit = limit;
  }
}
