import { Auto, AutoTab, Repeat } from "./utils/constant.js";

chrome.runtime.onInstalled.addListener(() => {
  let auto = new Auto(0, false, 2);
  auto.saveAuto();

  let repeat = new Repeat(0, 1, 500, [5, 5, 5], 0, 0, 0);
  repeat.saveRepeat();

  let autoTab = new AutoTab(0);
  autoTab.saveAutoTab();
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.message == "get_challenge") {
    executeScript(getChallenge, sendResponse);
  }
  if (request.message == "get_xp") {
    executeScript(getXP, sendResponse);
  }
  if (request.message == "get_cp") {
    executeScript(getCP, sendResponse);
  }
  return true;
});

function executeScript(func, sendResponse) {
  chrome.storage.local.get(["autoTab"], ({ autoTab }) => {
    chrome.scripting.executeScript(
      {
        target: { tabId: autoTab.tabId, allFrames: true },
        func: func,
        world: "MAIN",
      },
      (frames) => {
        if (!frames || !frames.length) return;
        const result = frames[0].result;
        sendResponse(result);
      }
    );
  });
}

function getChallenge() {
  const dom = document.querySelector("._1XNQX");
  if (dom) {
    const key = Object.keys(dom).find((key) => key.startsWith("__reactProps"));
    return dom[key].children._owner.memoizedProps.currentChallenge;
  }
}

function getXP() {
  let dom = document.querySelector("._23WOw");
  if (dom) {
    let key = Object.keys(dom).find((k) => k.startsWith("__reactProps"));
    return dom[key].children[2].props.children[2].props.result;
  }
}

function getCP() {
  let dom = document.querySelector("._23WOw");
  if (dom) {
    let key = Object.keys(dom).find((k) => k.startsWith("__reactProps"));
    return dom[key].children[1].props.result;
  }
}

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
  chrome.storage.local.get(["auto", "autoTab"], ({ auto, autoTab }) => {
    if (tabId == autoTab.tabId) {
      Auto.create(auto).setIsAuto(false).saveAuto();
    }
  });
});

chrome.windows.onRemoved.addListener((windowId) => {
  chrome.storage.local.get(["auto"], ({ auto }) => {
    Auto.create(auto).setIsAuto(false).saveAuto();
  });
});
